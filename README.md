# LittleBook

LittleBook is a Flask-based web application designed to display and interact with Bible texts. The application supports loading different Bible translations, highlighting specific verses or parts of verses based on a `highlights.txt` file, and provides both dark and light themes for enhanced readability.

## Features

- **Dynamic Bible Loading**: Select and load different Bible translations from a dropdown menu.
- **Verse Highlighting**: Highlight specific verses, words, or ranges within verses.
- **Theme Switching**: Toggle between dark and light themes.
- **Persistent Preferences**: Retain the last selected Bible and theme across sessions.
- **Responsive Design**: Adjusts layout for various screen sizes and devices.

## Project Structure

- `app.py`: The main Flask application file that handles routing and server-side logic.
- `script.js`: Client-side JavaScript for handling user interactions and DOM manipulation.
- `styles.css`: CSS file for styling the application.
- `index.html`: Main HTML file for the application layout and structure.
- `example.json`: Example JSON file containing Bible data.
- `highlights.txt`: Text file containing highlight information.

## Getting Started

### Prerequisites

- Python 3.6 or higher
- Flask

### Installation

1. **Clone the repository:**

    ```sh
    git clone https://gitlab.com/revswim/littlebook.git
    cd littlebook
    ```

2. **Install the required packages:**

    ```sh
    pip install -r requirements.txt
    ```

### Running the Application

1. **Start the Flask server:**

    ```sh
    python app.py
    ```

2. **Access the application:**

    Open a web browser and go to `http://127.0.0.1:5000`.

### Highlighting Bible Passages

The `highlights.txt` file contains highlight information in the following format:

- `<color> Book Chapter:Verse` for single verse highlights.
- `<color> Book Chapter:Verse word` for single word highlights.
- `<color> Book Chapter:Verse start_word end_word` for ranges within a verse.
- `<color> Book Chapter1-Verse1 Chapter2-Verse2` for multi-verse highlights.
- `<color> Book Chapter1:Verse1-Word1 Chapter2:Verse2-Word2` for multi-line highlights starting and ending at specific words.

Example:
```txt
<red> John 3:16
<blue> Genesis 1:1 heaven
<green> Genesis 1:2 earth for
<yellow> 1 John 1-2
<purple> Genesis 1:3-5 God Night
