from flask import Flask, render_template, jsonify, request, make_response
import os
import re
import json

app = Flask(__name__)
bibles_directory = './bibles'
highlights_file = './highlights.txt'
loaded_bible_data = {}

@app.route('/')
def index():
    if not os.path.exists(bibles_directory):
        os.makedirs(bibles_directory)
    bible_files = [f for f in os.listdir(bibles_directory) if f.endswith('.json')]
    last_bible = request.cookies.get('last_bible', bible_files[0] if bible_files else "")
    return render_template('index.html', bible_files=bible_files, app_name="LittleBook", last_bible=last_bible)

@app.route('/load_bible/<bible_name>')
def load_bible(bible_name):
    global loaded_bible_data
    try:
        with open(os.path.join(bibles_directory, bible_name), 'r') as f:
            bible_data = json.load(f)
        loaded_bible_data = bible_data  # Store the loaded Bible data
        response = make_response(jsonify({"bible_data": bible_data}))
        response.set_cookie('last_bible', bible_name)
        return response
    except FileNotFoundError:
        return jsonify({'error': 'Bible file not found'})

@app.route('/load_highlights', methods=['GET'])
def load_highlights():
    highlights = []
    try:
        with open(highlights_file, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            if not line:
                continue

            # Check for partial line highlights with multi-line support
            partial_multi_match = re.match(r'<(\w+)> (\w+ \d+:\d+)-(\w+ \d+:\d+) (\w+) (\w+)', line)
            if partial_multi_match:
                color, start_ref, end_ref, start_word, end_word = partial_multi_match.groups()
                start_book_chapter, start_verse = start_ref.split(':')
                start_book, start_chapter = start_book_chapter.rsplit(' ', 1)
                end_book_chapter, end_verse = end_ref.split(':')
                end_book, end_chapter = end_book_chapter.rsplit(' ', 1)
                start_chapter, start_verse = int(start_chapter), int(start_verse)
                end_chapter, end_verse = int(end_chapter), int(end_verse)

                highlights.append({
                    'color': color,
                    'book': start_book,
                    'chapter': start_chapter,
                    'verse': start_verse,
                    'start_word': start_word,
                    'end_word': None
                })

                for chapter in range(start_chapter, end_chapter + 1):
                    verse_start = start_verse if chapter == start_chapter else 1
                    verse_end = end_verse if chapter == end_chapter else get_verse_count(start_book, chapter)
                    for verse in range(verse_start, verse_end + 1):
                        if chapter == start_chapter and verse == start_verse:
                            continue
                        if chapter == end_chapter and verse == end_verse:
                            highlights.append({
                                'color': color,
                                'book': end_book,
                                'chapter': chapter,
                                'verse': verse,
                                'start_word': None,
                                'end_word': end_word
                            })
                        else:
                            highlights.append({
                                'color': color,
                                'book': start_book,
                                'chapter': chapter,
                                'verse': verse
                            })

                continue

            # Check for partial line highlights
            partial_match = re.match(r'<(\w+)> (\w+ \d+:\d+) (\w+) (\w+)', line)
            if partial_match:
                color, ref, start_word, end_word = partial_match.groups()
                book_chapter, verse = ref.split(':')
                book, chapter = book_chapter.rsplit(' ', 1)
                highlights.append({
                    'color': color,
                    'book': book,
                    'chapter': int(chapter),
                    'verse': int(verse),
                    'start_word': start_word,
                    'end_word': end_word
                })
                continue

            # Check for single word highlights
            single_word_match = re.match(r'<(\w+)> (\w+ \d+:\d+) (\w+)', line)
            if single_word_match:
                color, ref, word = single_word_match.groups()
                book_chapter, verse = ref.split(':')
                book, chapter = book_chapter.rsplit(' ', 1)
                highlights.append({
                    'color': color,
                    'book': book,
                    'chapter': int(chapter),
                    'verse': int(verse),
                    'start_word': word,
                    'end_word': word
                })
                continue

            # Check for multi-line highlights
            match = re.match(r'<(\w+)> (\w+ \d+:\d+)(?:-(\d+))?', line)
            if match:
                color, start_ref, end_verse = match.groups()
                start_book_chapter, start_verse = start_ref.split(':')
                start_book, start_chapter = start_book_chapter.rsplit(' ', 1)
                start_chapter, start_verse = int(start_chapter), int(start_verse)
                end_verse = int(end_verse) if end_verse else start_verse

                for verse in range(start_verse, end_verse + 1):
                    highlights.append({
                        'color': color,
                        'book': start_book,
                        'chapter': start_chapter,
                        'verse': verse
                    })

                continue

            # Regular highlight
            match = re.match(r'<(\w+)> (\w+ \d+:\d+)', line)
            if match:
                color, ref = match.groups()
                book_chapter, verse = ref.split(':')
                book, chapter = book_chapter.rsplit(' ', 1)
                highlights.append({
                    'color': color,
                    'book': book,
                    'chapter': int(chapter),
                    'verse': int(verse)
                })

    except FileNotFoundError:
        print(f"{highlights_file} not found.")
    except Exception as e:
        print(f"Error reading {highlights_file}: {e}")
    return jsonify(highlights=highlights)

def get_verse_count(book, chapter):
    global loaded_bible_data
    verses = [verse for verse in loaded_bible_data['verses'] if verse['book'] == book and verse['chapter'] == chapter]
    return len(verses)

if __name__ == '__main__':
    app.run(debug=True)
