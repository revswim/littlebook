document.addEventListener('DOMContentLoaded', () => {
    const toggleThemeButton = document.getElementById('toggle-theme');
    const bibleSelector = document.getElementById('bible-selector');
    const bibleContent = document.getElementById('bible-content');

    let isDarkMode = false;
    let currentBible = null;

    const lightModeColors = {
        red: '#ffcccc',
        orange: '#ffd9b3',
        yellow: '#ffffcc',
        green: '#ccffcc',
        blue: '#cce5ff',
        indigo: '#d9ccff',
        violet: '#ffccff'
    };

    const darkModeColors = {
        red: '#cc0000',
        orange: '#e65c00',
        yellow: '#999900',
        green: '#006600',
        blue: '#004080',
        indigo: '#4b0082',
        violet: '#800080'
    };

    toggleThemeButton.addEventListener('click', () => {
        isDarkMode = !isDarkMode;
        document.body.classList.toggle('dark-mode', isDarkMode);
        document.body.classList.toggle('light-mode', !isDarkMode);
        localStorage.setItem('theme', isDarkMode ? 'dark' : 'light');
        updateHighlightColors();
    });

    bibleSelector.addEventListener('change', () => {
        const selectedBible = bibleSelector.value;
        fetch(`/load_bible/${selectedBible}`)
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    bibleContent.innerHTML = data.error;
                } else {
                    currentBible = data.bible_data;
                    displayAllChapters(currentBible.verses);
                    loadHighlights();
                }
            })
            .catch(error => console.error('Error loading Bible:', error));
    });

    function displayAllChapters(verses) {
        bibleContent.innerHTML = '';
        const chapters = {};

        verses.forEach(verse => {
            const key = `${verse.book}-${verse.chapter}`;
            if (!chapters[key]) {
                chapters[key] = [];
            }
            chapters[key].push(verse);
        });

        Object.keys(chapters).forEach(key => {
            const chapterDiv = document.createElement('div');
            chapterDiv.classList.add('chapter');
            const [book, chapter] = key.split('-');
            chapterDiv.innerHTML = `<h2>${book} ${chapter}</h2>`;

            chapters[key].forEach(verse => {
                const verseElement = document.createElement('div');
                verseElement.classList.add('verse');
                verseElement.id = `${verse.book}-${verse.chapter}-${verse.verse}`;
                verseElement.setAttribute('contenteditable', 'false');
                verseElement.innerHTML = `<strong>${verse.verse}</strong>  <span class="verse-text">${verse.text}</span>`;
                chapterDiv.appendChild(verseElement);
            });

            bibleContent.appendChild(chapterDiv);
        });
    }

    function loadHighlights() {
        fetch('/load_highlights')
            .then(response => response.json())
            .then(data => {
                const highlights = data.highlights;
                applyHighlights(highlights);
            })
            .catch(error => console.error('Error loading highlights:', error));
    }

    function applyHighlights(highlights) {
        highlights.forEach(highlight => {
            const { color, book, chapter, verse, start_word, end_word } = highlight;

            const verseElement = document.getElementById(`${book}-${chapter}-${verse}`);
            if (verseElement) {
                const verseText = verseElement.querySelector('.verse-text');
                if (verseText) {
                    const highlightColor = getHighlightColor(color);
                    if (start_word && end_word) {
                        const text = verseText.textContent;
                        const startIndex = text.indexOf(start_word);
                        const endIndex = text.indexOf(end_word) + end_word.length;
                        if (startIndex !== -1 && endIndex !== -1) {
                            const highlightedText = `<span style="background-color: ${highlightColor};">${text.substring(startIndex, endIndex)}</span>`;
                            verseText.innerHTML = text.substring(0, startIndex) + highlightedText + text.substring(endIndex);
                        }
                    } else {
                        verseText.style.backgroundColor = highlightColor;
                    }
                    console.log(`Highlighted ${book} ${chapter}:${verse} with ${highlightColor}`);
                }
            } else {
                console.warn(`Verse element not found for ${book} ${chapter}:${verse}.`);
            }
        });
    }

    function getHighlightColor(color) {
        const lowerColor = color.toLowerCase();
        const lightColorHex = lightModeColors[lowerColor];
        const darkColorHex = darkModeColors[lowerColor];
        if (isDarkMode && darkColorHex) {
            return darkColorHex;
        } else if (!isDarkMode && lightColorHex) {
            return lightColorHex;
        }
        return color;
    }

    function updateHighlightColors() {
        const allVerses = document.querySelectorAll('.verse-text');
        allVerses.forEach(verse => {
            let currentColor = verse.style.backgroundColor;
            currentColor = rgbToHex(currentColor);
            const colorName = Object.keys(lightModeColors).find(
                key => lightModeColors[key] === currentColor || darkModeColors[key] === currentColor
            );
            if (colorName) {
                verse.style.backgroundColor = getHighlightColor(colorName);
            }
            verse.querySelectorAll('span').forEach(span => {
                let spanColor = span.style.backgroundColor;
                spanColor = rgbToHex(spanColor);
                const spanColorName = Object.keys(lightModeColors).find(
                    key => lightModeColors[key] === spanColor || darkModeColors[key] === spanColor
                );
                if (spanColorName) {
                    span.style.backgroundColor = getHighlightColor(spanColorName);
                }
            });
        });
    }

    function rgbToHex(rgb) {
        const result = /^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/.exec(rgb);
        return result ? "#" +
            ("0" + parseInt(result[1], 10).toString(16)).slice(-2) +
            ("0" + parseInt(result[2], 10).toString(16)).slice(-2) +
            ("0" + parseInt(result[3], 10).toString(16)).slice(-2) : rgb;
    }

    const lastBible = getCookie('last_bible');
    if (lastBible) {
        bibleSelector.value = lastBible;
        const event = new Event('change');
        bibleSelector.dispatchEvent(event);
    }

    const savedTheme = localStorage.getItem('theme');
    if (savedTheme) {
        isDarkMode = savedTheme === 'dark';
        document.body.classList.toggle('dark-mode', isDarkMode);
        document.body.classList.toggle('light-mode', !isDarkMode);
        updateHighlightColors();
    }

    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }
});
